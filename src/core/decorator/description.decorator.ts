export function Description(name: string) {
  return (target: any, propertyKey: string | symbol) => {
    const metaValue = target.$description === undefined ? {} : target.$description;
    metaValue[propertyKey] = name;

    Object.defineProperty(target, '$description', {
      value: metaValue,
      configurable: true,
    });
  };
}
