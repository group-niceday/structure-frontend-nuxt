import Vue from 'vue';

import {Enum} from '@/src/app/shared/model/enum.model';

export class Broadcast extends Vue {
  onSpinner(type: Enum.CORE.SPINNER, useYn: boolean) {
    this.$nextTick(() => {
      this.$broadcast.$emit(type, useYn);
    });
  }

  onError(error: any) {
    this.$nextTick(() => {
      this.$broadcast.$emit(
        Enum.CORE.SPINNER.SPINNER_ERROR,
        error
      );
    });
  }

  onValidate(error: any) {
    console.log(error);
    this.$broadcast.$emit(
      Enum.CORE.TOASTER.DEFAULT,
      error
    );
  }
}
