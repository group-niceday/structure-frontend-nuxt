import Vue from 'vue';
import * as validator from 'class-validator';

import UtilService from '@/src/app/shared/service/util.service';
import {Mapper} from '@/src/core/service/mapper.service';
import {Broadcast} from '@/src/core/service/broadcast.service';

declare module 'vue/types/vue' {


  interface Vue {
    $mq: string;
    $enum: any;
    $util: UtilService;
    $broadcast: Broadcast;
    $mapper: Mapper;
    $validator: validator.Validator;
  }

  interface VueConstructor {
    $mq: string;
    $enum: any;
    $util: UtilService;
    $broadcast: Broadcast;
    $mapper: Mapper;
    $validator: validator.Validator;
  }
}

declare module 'vue/types/options' {
  interface ComponentOptions<V extends Vue> {
    $mq?: string;
    $enum?: any;
    $util?: UtilService;
    $broadcast?: Broadcast;
    $mapper?: Mapper;
    $validator?: validator.Validator;
  }
}
