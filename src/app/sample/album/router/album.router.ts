import {RouteConfig} from '@nuxt/types/config/router';

import Default from '@/src/app/system/layout/default.vue';
import AlbumList from '@/src/app/sample/album/view/album-list.vue';
import AlbumDetail from '@/src/app/sample/album/view/album-detail.vue';
import AlbumAdd from '@/src/app/sample/album/view/album-add.vue';

const router: RouteConfig[] = [
  {
    path: '/default/album',
    component: Default,
    redirect: '/default/album/list',
    children: [
      {
        path: 'list',
        component: AlbumList,
      },
      {
        path: 'detail/:id',
        component: AlbumDetail,
      },
      {
        path: 'add',
        component: AlbumAdd,
      }
    ]
  }
];

export default router;
