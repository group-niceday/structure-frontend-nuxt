import {Album} from '@/src/app/sample/album/model/album.model';

export namespace OtherServer {
  export const Index = {
    async asyncData({store}: any) {
      await store.dispatch('global/setToken', 'TEST_GLOBAL_SAMPLE');

      console.log('SERVER GLOBAL: ', store.getters['global/getToken']());
      return {};
    }
  };

  export const Middleware = {
    middleware: ['check.middleware']
  }

  export const Server = {
    async asyncData({store}: any) {
      try {
        // await store.dispatch('user/getUsers');
        // console.log('SERVER GLOBAL: ', store.getters['global/getToken']());
        // await store.dispatch('user/getUser', '1');
        await store.dispatch('album/setAlbum', new Album.Request.Add());
      } catch (error) {
        console.log('ERROR', error);
      }

      return {
        // users: store.getters['user/getUsers'](),
        // user: store.getters['user/getUser']()
      };
    }
  }
}
