export namespace Enum {
  export namespace SAMPLE {
    export namespace ALBUM {
      export enum TEST {
        TEST_1 = 'TEST_1',
        TEST_2 = 'TEST_2'
      }
    }
  }

  export namespace CORE {
    export enum ENV {
      LOCAL = 'local',
      DEV = 'development',
      PROD = 'production'
    }

    export namespace STORAGE {
      export enum KEY {
        TEST = 'TEST'
      }

      export enum TYPE {
        STRING = 'string',
        NUMBER = 'number',
        BOOLEAN = 'boolean',
        OBJECT = 'object',
        ARRAY = 'array',
      }
    }

    export enum SPINNER {
      DEFAULT = 'DEFAULT',
      WRITE = 'WRITE',
      SPINNER_ERROR = 'SPINNER_ERROR'
    }

    export enum TOASTER {
      DEFAULT = 'DEFAULT',
    }

    export enum DATE_FORMAT {
      RETURN_DATE_TIME = 'YYYY-MM-DDTHH:mm',
      RETURN_DATE_TIME_SECOND = 'YYYY-MM-DDTHH:mm:ss',

      DISPLAY_DATE_TIME = 'YYYY-MM-DD HH:mm',
      DISPLAY_DATE_TIME_SECOND = 'YYYY-MM-DD HH:mm:ss'
    }
  }
}
