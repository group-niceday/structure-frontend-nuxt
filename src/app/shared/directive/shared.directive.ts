import Vue, {VNode} from 'vue';
import {DirectiveBinding} from 'vue/types/options';

Vue.directive('numericOnly', {
  bind(el: HTMLElement, binding: DirectiveBinding, vnode: VNode) {
    el.addEventListener('keydown', (e: KeyboardEvent) => {
      if ([46, 8, 9, 27, 13, 110, 190].indexOf(e.keyCode) !== -1 || (e.keyCode === 65 && e.ctrlKey) || (e.keyCode === 67 && e.ctrlKey) || (e.keyCode === 88 && e.ctrlKey) ||
        // Allow: home, end, left, right
        (e.keyCode >= 35 && e.keyCode <= 39)) {
        // let it happen, don't do anything
        return;
      }
      // Ensure that it is a number and stop the keypress
      if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
        e.preventDefault();
      }
    });
  }
});

const checkMaxlength = (el: HTMLElement, binding: DirectiveBinding, e: KeyboardEvent) => {
  const input = el as HTMLInputElement;

  if (!!input.value) {
    if (input.value.length > binding.value.maxLength) {
      setTimeout(() => {
        binding.value.parent[binding.value.model] = input.value.substr(0, binding.value.maxLength);
      });
    }
  }
};

Vue.directive('maxLength', {
  bind(el: HTMLElement, binding: DirectiveBinding, vnode: VNode) {
    el.addEventListener('keyup', (e: KeyboardEvent) => {
      checkMaxlength(el, binding, e);
    });

    el.addEventListener('keydown', (e: KeyboardEvent) => {
      checkMaxlength(el, binding, e);
    });
  }
});