import Vue from 'vue';

Vue.filter('testFilter', (value: string, nullTag?: string) => {
  return `${value}-test`;
});
