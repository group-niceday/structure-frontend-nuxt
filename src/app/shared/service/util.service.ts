import get from 'lodash/get';
import moment from 'moment-timezone';
import {Enum} from '@/src/app/shared/model/enum.model';

export default class UtilService {
  safeCall(parent: any, key: string) {
    return get(parent, key);
  }

  dateToFormatDate(date: Date | string, format: Enum.CORE.DATE_FORMAT = Enum.CORE.DATE_FORMAT.DISPLAY_DATE_TIME): string {
    return moment(date).format(format);
  }

  setNumberToString(num: number): string {
    let returnValue = '';

    if (num !== null && num !== undefined) {
      if (num >= 10) {
        returnValue = num.toString();
      } else {
        returnValue = `0${num}`;
      }
    }

    return returnValue;
  }

  getRandomString(length: number) {
    let result: string = '';
    const characters: string = '0123456789';
    const charactersLength: number = characters.length;

    for (let i = 0; i < length; i++) {
      result += characters.charAt(Math.floor(Math.random() * charactersLength));
    }

    return `Random_${result}`;
  }
}
