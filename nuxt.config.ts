import bodyParser from 'body-parser';

export default {
  head: {
    title: 'structure-frontend-nuxt',
    htmlAttrs: {
      lang: 'ko'
    },
    meta: [
      {charset: 'utf-8'},
      {name: 'viewport', content: 'width=device-width, initial-scale=1'},
      {hid: 'description', name: 'description', content: 'TEST_SHARED_DESCRIPTION'},
      {property: 'og:title', content: 'SHARED_TITLE'},
      {property: 'og:type', content: 'website'},
      {property: 'og:url', content: 'http://local.genie.co.kr'},
      {property: 'og:url', content: 'http://local.genie.co.kr/facebook_1200x630.jpeg'},
      {property: 'og:image:secure_url', content: 'http://local.genie.co.kr/facebook_1200x630.jpeg'},
      {property: 'og:description', content: 'TEST_SHARED_DESCRIPTION'},
      {
        name   : 'appleid-signin-client-id',
        content: `kr.co.geniemusic.stayg.servicesid`
      },
      {
        name   : 'appleid-signin-scope',
        content: `name email`
      },
      {
        name   : 'appleid-signin-redirect-uri',
        content: `https://dev.niceday.io/callback`
      },
      // {
      //   name   : 'appleid-signin-state',
      //   content: `${process.env.APPLE_STATE}`
      // },
      {
        name   : 'appleid-signin-use-popup',
        content: 'false'
      }
    ],
    link: [
      {rel: 'icon', type: 'image/x-icon', href: '/favicon.ico'}
    ],
    script: [
      {
        type: 'text/javascript',
        src : 'https://appleid.cdn-apple.com/appleauth/static/jsapi/appleid/1/en_US/appleid.auth.js'
      }
    ],
  },
  css: [
    '@/src/assets/css/env/' + process.env.VUE_APP_ENV + '.scss',
    'swiper/css/swiper.css'
  ],
  plugins: [
    '@/plugins/main',
    '@/store',
    '@/store/store.persistedstate',
    '@/plugins/axios.interceptor',
    '@/plugins/i18n.interceptor',
  ],
  components: true,

  buildModules: [
    '@nuxt/typescript-build',
    ['@nuxtjs/router', {fileName: 'plugins/router.ts'}],
    ['@nuxtjs/dotenv', {filename: '.env.' + process.env.VUE_APP_ENV}],
  ],
  modules: [
    '@nuxtjs/axios',
    '@nuxtjs/auth-next',
    ['nuxt-mq', {
      defaultBreakpoint: 'mobile',
      breakpoints: {
        mobile: 1320,
        desktop: Infinity
      }
    }],
    ['nuxt-i18n', {
      locales: [
        {name: '한국어', code: 'ko', iso: 'ko-KR', file: 'ko.ts'},
        {name: 'English', code: 'en', iso: 'en-US', file: 'en.ts'}
      ],
      lazy: true,
      langDir: 'plugins/lang/',
      strategy: 'no_prefix',
      defaultLocale: 'en',
      detectBrowserLanguage: {
        useCookie: true,
        cookieKey: 'i18n_redirected',
        alwaysRedirect: true
      },
      rootRedirect: 'en',
      vueI18n: {
        fallbackLocale: 'en'
      }
    }]
  ],
  build: {
    maxChunkSize: 300000,
    extractCSS: true,
    extend(config: any) {
      config.devtool = '#source-map'
    }
  },
  serverMiddleware: [
    bodyParser.urlencoded({extended: true})
  ],
  auth: {
    redirect: {
      // callback: '/callback',
      logout: '/signed-out'
    },
    strategies: {
      google: {
        responseType: 'code',
        codeChallengeMethod: '',
        grantType: 'authorization_code',
        clientId: '103772106146-2p1fv3mj29k6rtco1vd2trrd4ru64t29.apps.googleusercontent.com',
        clientSecret: 'JjJBj5lVRPS6jSOM5PBpSO87',
        scope: ['openid', 'profile', 'email'],
        redirectUri : 'https://dev.niceday.io/callback?mode=google',
      },
      facebook: {
        responseType: 'code',
        redirectUri : 'https://dev.niceday.io/callback?mode=facebook',
        endpoints: {
          userInfo: 'https://graph.facebook.com/v6.0/me?fields=id,name,picture{url}'
        },
        clientId: '250638366467846',
        scope: ['public_profile', 'email']
      },
    }
  }
}
