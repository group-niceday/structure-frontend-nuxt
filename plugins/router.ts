import Vue from 'vue';
import Router from 'vue-router';

import AlbumRouter from '@/src/app/sample/album/router/album.router';
import UserRouter  from '@/src/app/sample/user/router/user.router';
import OtherRouter from '@/src/app/sample/other/router/other.router';
import AppleRouter from '@/src/app/sample/apple/router/apple.router';
import Callback    from '@/src/app/system/view/callback.vue';
import SignedOut   from '@/src/app/system/view/signed-out.vue';

Vue.use(Router);

export function createRouter() {
  return new Router({
    mode: 'history',
    routes: [
      {
        path: '/',
        redirect: '/default/album/list'
      },
      {
        path: '/callback',
        component: Callback,
      },
      {
        path: '/signed-out',
        component: SignedOut,

      },
      ...AlbumRouter,
      ...UserRouter,
      ...OtherRouter,
      ...AppleRouter
    ]
  });
}
