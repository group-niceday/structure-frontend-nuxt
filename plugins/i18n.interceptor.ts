export default function({app}: any) {
  app.i18n.beforeLanguageSwitch = (oldLocale: any, newLocale: any) => {
    console.log(oldLocale, newLocale);
  }

  app.i18n.onLanguageSwitched = (oldLocale: any, newLocale: any) => {
    console.log(oldLocale, newLocale);
  }
}
