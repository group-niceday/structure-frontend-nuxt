import SharedEn from '@/src/app/shared/lang/shared.en';
import AlbumEn from '@/src/app/sample/album/lang/album.en';

export default {
  shared: SharedEn,
  album: AlbumEn,
  user: 'User',
}
