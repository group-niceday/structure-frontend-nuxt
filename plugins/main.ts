import Vue from 'vue';
import * as validator from 'class-validator';

import Components from '@/plugins/components';

import {Enum} from '@/src/app/shared/model/enum.model';
import {Mapper} from '@/src/core/service/mapper.service';
import {Broadcast} from '@/src/core/service/broadcast.service';
import UtilService from '../src/app/shared/service/util.service';

import '@/src/app/shared/filter/shared.filter';
import '@/src/app/shared/directive/shared.directive';

Vue.config.devtools = process.env.NODE_ENV === Enum.CORE.ENV.LOCAL;
Vue.config.performance = process.env.NODE_ENV === Enum.CORE.ENV.LOCAL;
Vue.config.silent = process.env.NODE_ENV === Enum.CORE.ENV.PROD;
Vue.config.productionTip = false;

Vue.prototype.$enum = Enum;
Vue.prototype.$util = new UtilService();
Vue.prototype.$broadcast = new Broadcast();
Vue.prototype.$mapper = new Mapper();
Vue.prototype.$validator = new validator.Validator();

Vue.use(Components);
