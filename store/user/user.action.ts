import {AxiosResponse} from 'axios';
import {Actions} from 'vuex-smart-module';

import {User} from '@/src/app/sample/user/model/user.model';
import {UserState} from '@/store/user/user.state';
import {UserGetter} from '@/store/user/user.getter';
import {UserMutation} from '@/store/user/user.mutation';
import AxiosService from '@/src/app/shared/service/axios.service';

export class UserAction extends Actions<UserState, UserGetter, UserMutation, UserAction> {
  private http = new AxiosService('https://jsonplaceholder.typicode.com');

  getUsers() {
    return this.http.get(
      `/users`
    ).then(
      (response: AxiosResponse<User.Response.FindAll[]>) =>
        this.mutations.setUsers(response.data)
    );
  }

  getUser(id: string | number) {
    return this.http.get(
      `/users/${id}`
    ).then(
      (response: AxiosResponse<User.Response.FindOne>) =>
        this.mutations.setUser(response.data)
    );
  }
}
