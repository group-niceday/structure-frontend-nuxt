import {Module} from 'vuex-smart-module';

import {UserState} from '@/store/user/user.state';
import {UserGetter} from '@/store/user/user.getter';
import {UserMutation} from '@/store/user/user.mutation';
import {UserAction} from '@/store/user/user.action';

export default new Module({
  state: UserState,
  getters: UserGetter,
  mutations: UserMutation,
  actions: UserAction,
});
