import {Getters} from 'vuex-smart-module';

import {User} from '@/src/app/sample/user/model/user.model';
import {UserState} from '@/store/user/user.state';
import {Mapper} from '@/src/core/service/mapper.service';

export class UserGetter extends Getters<UserState> {
  mapper = new Mapper();

  getUsers(): User.Response.FindAll[] {
    return this.mapper.toArray(User.Response.FindAll, this.state.users);
  }

  getUser(): User.Response.FindOne {
    return this.mapper.toObject(User.Response.FindOne, this.state.user);
  }
}
