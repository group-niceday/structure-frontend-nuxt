import {User} from '@/src/app/sample/user/model/user.model';

export class UserState {
  users: User.Response.FindAll[] = [];
  user: User.Response.FindOne = new User.Response.FindOne;
}
