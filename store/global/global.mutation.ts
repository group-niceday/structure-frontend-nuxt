import {Mutations} from 'vuex-smart-module';

import {GlobalState} from '@/store/global/global.state';

export class GlobalMutation extends Mutations<GlobalState> {
  setToken(token: string) {
    this.state.token = !!token ? `${token}` : '';
  }
}
