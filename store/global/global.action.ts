import {Actions} from 'vuex-smart-module';

import {GlobalState} from '@/store/global/global.state';
import {GlobalGetter} from '@/store/global/global.getter';
import {GlobalMutation} from '@/store/global/global.mutation';

export class GlobalAction extends Actions<GlobalState, GlobalGetter, GlobalMutation, GlobalAction> {
  setToken(token: string) {
    return this.mutations.setToken(token);
  }
}
