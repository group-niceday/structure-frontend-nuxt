# 프론트엔드 개발 기준(structure-frontend-nuxt)
* frontend 개발을 위한 기본 개발환경을 제공한다.


# 이력
* v0.0.1
    * 2021.03.23
        * 최초 등록

# 구성
* 환경
    * windows 10, mac
    * webstorm 2019.3.2
    * node v12.14.2
    * npm v6.13.4
    * vue-cli v4.1.2

<!-- blank line -->
* 라이브러리
    * nuxt
    * @nuxt/types
        * https://github.com/nuxt/nuxt.js
    * @nuxtjs/dotenv
        * https://github.com/nuxt-community/dotenv-module
    * @nuxtjs/router
        * https://github.com/nuxt-community/router-module
    * @nuxtjs/axios
        * https://github.com/nuxt-community/axios-module
    * @nuxtjs/auth-next
        * https://github.com/nuxt-community/auth-module
    * nuxt-i18n
        * https://github.com/nuxt-community/i18n-module
    * nuxt-property-decorator
        * https://github.com/nuxt-community/nuxt-property-decorator
    * class-validator
        * https://github.com/typestack/class-validator
    * class-transformer
        * https://github.com/typestack/class-transformer
    * vuex
        * https://github.com/vuejs/vuex
    * vuex-smart-module
        * https://github.com/ktsn/vuex-smart-module
    * vuex-persistedstate
        * https://github.com/robinvdvleuten/vuex-persistedstate
    * @types/lodash
        * https://github.com/lodash/lodash
    * qs
        * https://github.com/ljharb/qs

# 설치 및 실행
<!-- blank line -->
* 설치
    * Node 모듈 설치 (<a href="https://nodejs.org/ko/">Link</a>)
    * Git 설치 (<a href="https://git-scm.com/">Link</a>)
    * vue-cli 설치 (<a href="https://cli.vuejs.org/">Link</a>)
    * Webstrom 설치 (<a href="https://www.jetbrains.com/ko-kr/webstorm/download/#section=windows">Link</a>)

* 실행
    * npm install
    * npm run serve:local

## WebStorm
* 코드 spaces 셋팅
    * Preferences > Editor > Code Style
    * HTML > Tabs and Indents
        * Tab Size : 2
        * Indent : 2
        * Continuation Indent: 4
    * Typescript > Tabs and Indents
        * Tab Size : 2
        * Indent : 2
        * Continuation Indent: 2
    * Typescript > Punctuation
        * use "Single" quotes "in new Code"
* Key Mapping 셋팅
    * "Reformat Code" 항목 단축키 등록

# 개발
<!-- blank line -->
## 패키지 및 파일 명명 규칙
* 모든 화면 파일명(*.vue)은 "kebab-case"로 작성한다.
    * Good: sample-list.vue
    * Bad: SampleList.vue
* 모든 화면 파일명(*.vue)은 기능의 해당하는 두가지 단어 이상으로 작성한다.
    * Good: sample-list.vue
    * Bad: sample.vue
* 모든 기능 파일명(*.ts)은 "kebab-case" + "dot.case"로 작성한다.
    * Good: platform.service.ts
    * Bad: platform-service.ts
    * 기능 ex) service, model, router, service...
* 모든 폴더는 "kebab-case"로 작성한다.
    * Good: platform, tv-platform
    * Bad: TvPlatform, tvPlatform
* 기타 파일은 "kebab-case"로 작성한다.
    * Good: style.css, default-style.css
    * Bad: defaultStyle.css

## 패키지 구조
  ```
  ├── middleware
  ├── pages
  ├── plugins
  │   ├── lang
  │   │   ├── en.ts
  │   │   └── kr.ts
  │   ├── axios.interceptor.ts
  │   ├── i18n.interceptor.ts
  │   ├── components.ts
  │   ├── router.ts
  │   └── main.ts
  ├── src
  │   ├── app
  │   │   ├── {{대분류}}
  │   │   │   └── {{중분류}}
  │   │   │       └── {{소분류}}
  │   │   │           ├── components
  │   │   │           ├── config
  │   │   │           ├── model
  │   │   │           ├── router
  │   │   │           ├── server
  │   │   │           ├── service
  │   │   │           └── view
  │   │   ├── system
  │   │   │   ├── layout
  │   │   │   └── view
  │   │   └── shared
  │   │       ├── components
  │   │       ├── config
  │   │       ├── directive
  │   │       ├── filter
  │   │       ├── lang
  │   │       ├── model
  │   │       ├── server
  │   │       └── service
  │   ├── assets
  │   │   ├── css
  │   │   ├── font
  │   │   └── images
  │   │       ├── common
  │   │       └── icon
  │   ├── modules
  │   │   └── @types
  │   │       └── @{{명칭}}
  │   └── core
  │       ├── decorator
  │       │   ├── description.decorator.ts
  │       │   └── validate.decorator.ts
  │       └── service
  │           ├── description.decorator.ts
  │           └── validate.decorator.ts
  ├── static
  ├── store
  │   │   └── {{대분류}}
  │   │       └── {{중분류}}
  │   │           └── {{소분류}}
  │   │               ├── {{소분류}}.action.ts
  │   │               ├── {{소분류}}.getter.ts
  │   │               ├── {{소분류}}.mutation.ts
  │   │               ├── {{소분류}}.state.ts
  │   │               └── {{소분류}}.module.ts
  │   ├── store.persistedstate.ts
  │   └── index.ts
  ├── .gitignore
  ├── env.development
  ├── env.product
  ├── env.local
  ├── package.json
  ├── readme.md
  ├── tsconfig.json
  └── tslint.json
  ```
## 상위 패키지 구조 설명
| 패키지          | 설명 |
| --------      |  -------- |
| /middleware   | Nuxt 미들웨어 구현영역 |
| /pages        | Nuxt 페이지 구현영역(사용안함) |
| /plugins      | Nuxt 플러그인 구현영역(사용안함) |
| /src          | 화면 구현영역 |
| /static       | 정적 시스템 저장영역 |
| /store        | Vuex 구현영역 |

## 패키지 구조 설명
| 패키지                | 설명 |
| --------            |  -------- |
| /plugins/lang       | i18n 다국어 폴더 |
| /src/assets         | 스타일 구현영역 |
| /src/modules        | typescript 지원영역 |
| /src/core/decorator | 전역 decorator 구현 영역 |
| /src/core/service   | 전역 service 구현 영역 |
| /src/app/하위메뉴     | 화면 영역 |
| /src/app/system     | 시스템 화면영역 |
| /src/app/shared     | 공통 영역 |
| /store/하위메뉴       | vuex store 관련 영역 |

## 화면 별 패키지 구조
  ```
  ├── components
  ├── config
  ├── model
  ├── router
  ├── server
  ├── service
  └── view
  ```

## 화면 별 패키지 구조 설명
| 패키지          | 설명 |
|  --------      |  -------- |
| /components    | 컴포넌트 영역 |
| /config        | 설정 영역 |
| /model         | 모델 영역 |
| /router        | 라우터 영역 |
| /server        | 서버 영역 |
| /service       | 서비스 영역 |
| /view          | 화면 영역 |

# 코딩 명명 규칙
## 함수명
| 함수명          | 설명 |
|  --------      |  -------- |
| onAdd          | 등록 페이지 이동 |
| onModify       | 수정 페이지 이동 |
| onCancel       | 이전 페이지 이동 |
| onRow          | 목록 테이블 ROW 데이터 클릭 |
| onSave         | 등록 FORM Action |
| onUpdate       | 수정 FORM Action |
| onDelete       | 삭제 FORM Action |
| onSearch       | 검색 FORM Action |

## Action
| Action 명       | 설명 |
|  --------       |  -------- |
| getAll          | 목록 조회 |
| getOne          | 상세 조회 |
| getPage         | 목록 페이지 조회 |
| add             | 등록 |
| update          | 수정 |
| delete          | 삭제 |

## 접근제어자
* 모든 public 접근제어자는 생략한다.
* 화면에서 사용하는 변수, 함수는 public 이다.
* class 내부적으로 사용하는 함수, 변수는 private를 명시적으로 선언한다.

## 명명규칙
| 단위            | 설명 |
|  --------      |  -------- |
| variable       | camelCase |
| config, enum   | SCREAMING SNAKE CASE |
| function       | camelCase |
| class          | PascalCase |
| folder         | kebab-case |
| *.ts           | kebab-case + dot.case + .ts |
| *.css          | kebab-case + .* |
| *.vue          | kebab-case + .vue |
| *.other        | kebab-case + .* |


# 예제
## enum
* 작성언어: typescript
* enum 변수는 `SCREAMING_SNAKE_CASE`로 작성한다.
```typescript
export namespace Enum {
  export namespace SAMPLE {
    export namespace ALBUM {
      export enum TEST {
        TEST_1 = 'TEST_1',
        TEST_2 = 'TEST_2'
      }
    }
  }

  export namespace CORE {
    export enum ENV {
      LOCAL = 'local',
      DEV = 'development',
      PROD = 'production'
    }

    export namespace STORAGE {
      export enum KEY {
        TEST = 'TEST'
      }

      export enum TYPE {
        STRING = 'string',
        NUMBER = 'number',
        BOOLEAN = 'boolean',
        OBJECT = 'object',
        ARRAY = 'array',
      }
    }

    export enum SPINNER {
      DEFAULT = 'DEFAULT',
      WRITE = 'WRITE',
      SPINNER_ERROR = 'SPINNER_ERROR'
    }

    export enum DATE_FORMAT {
      RETURN_DATE_TIME = 'YYYY-MM-DDTHH:mm',
      RETURN_DATE_TIME_SECOND = 'YYYY-MM-DDTHH:mm:ss',

      DISPLAY_DATE_TIME = 'YYYY-MM-DD HH:mm',
      DISPLAY_DATE_TIME_SECOND = 'YYYY-MM-DD HH:mm:ss'
    }
  }
}
```

## model
* 작성언어: typescript
* Request / Response - API 요청, 수신 기준으로 Request / Response로 분할하여 작성
* Request Validator는 `class-validator` 를 사용하여 작성
* Object, Array의 하위항목은 `@ValidateNested`를 사용하여 작성
* 필수항목은 `@IsNotEmpty`, 선택항목은 `@IsOptional`을 사용함
```typescript
import 'reflect-metadata';
import {Expose} from 'class-transformer';
import {IsNotEmpty, IsNumber, IsString} from 'class-validator';

import {Description} from '@/src/core/decorator/description.decorator';

export namespace Album {
  export namespace Request {
    export class Add {
      @Expose() @Description('사용자-아이디')
      @IsNumber() @IsNotEmpty()
      userId!: number;

      @Expose() @Description('제목')
      @IsString() @IsNotEmpty()
      title!: string;
    }
  }

  export namespace Response {
    export class FindAll {
      @Expose() @Description('사용자-아이디')
      userId!: number;

      @Expose() @Description('아이디')
      id!: number;

      @Expose() @Description('제목')
      title!: string;
    }

    export class FindOne {
      @Expose() @Description('사용자-아이디')
      userId!: number;

      @Expose() @Description('아이디')
      id!: number;

      @Expose() @Description('제목')
      title!: string;
    }
  }
}
```

## router
* 작성언어: typescript
* 화면 router 정보 사용
```typescript
import {RouteConfig} from '@nuxt/types/config/router';

import Default from '@/src/app/system/layout/default.vue';
import AlbumList from '@/src/app/sample/album/view/album-list.vue';
import AlbumDetail from '@/src/app/sample/album/view/album-detail.vue';
import AlbumAdd from '@/src/app/sample/album/view/album-add.vue';

const router: RouteConfig[] = [
  {
    path: '/default/album',
    component: Default,
    redirect: '/default/album/list',
    children: [
      {
        path: 'list',
        component: AlbumList,
      },
      {
        path: 'detail/:id',
        component: AlbumDetail,
      },
      {
        path: 'add',
        component: AlbumAdd,
      }
    ]
  }
];

export default router;
```

## router - plugins
* 작성언어: typescript
* 작성한 router를 추가
```typescript
import Vue from 'vue';
import Router from 'vue-router';

import AlbumRouter from '@/src/app/sample/album/router/album.router';
import UserRouter from '@/src/app/sample/user/router/user.router';
import OtherRouter from '@/src/app/sample/other/router/other.router';
import Callback from '@/src/app/system/view/callback.vue';
import SignedOut from '@/src/app/system/view/signed-out.vue';

Vue.use(Router);

export function createRouter() {
  return new Router({
    mode: 'history',
    routes: [
      {
        path: '/',
        redirect: '/default/album/list'
      },
      {
        path: '/callback',
        component: Callback,
      },
      {
        path: '/signed-out',
        component: SignedOut,

      },
      ...AlbumRouter,
      ...UserRouter,
      ...OtherRouter
    ]
  });
}

```

## view
* 작성언어: vue, typescript
* 화면은 page, model, 화면으로 작성함
```vue
<template>
  <div>
    <h1>OtherMiddlewareAfter</h1>
  </div>
</template>

<script lang="ts">
import Vue from 'vue'
import {Component} from 'nuxt-property-decorator';

@Component
export default class OtherMiddlewareAfter extends Vue {}
</script>
```

## vuex - action
* 작성언어: typescript
* vuex 사용
* Validate 사용을 원하는 action은 `@Validate`사용
```typescript
import {AxiosResponse} from 'axios';
import {Actions} from 'vuex-smart-module';

import {User} from '@/src/app/sample/user/model/user.model';
import {UserState} from '@/store/user/user.state';
import {UserGetter} from '@/store/user/user.getter';
import {UserMutation} from '@/store/user/user.mutation';
import AxiosService from '@/src/app/shared/service/axios.service';

export class UserAction extends Actions<UserState, UserGetter, UserMutation, UserAction> {
  private http = new AxiosService('https://jsonplaceholder.typicode.com');

  getUsers() {
    return this.http.get(
      '/users'
    ).then(
      (response: AxiosResponse<User.Response.FindAll[]>) =>
        this.mutations.setUsers(response.data)
    );
  }

  getUser(id: string | number) {
    return this.http.get(
      '/users/' + id
    ).then(
      (response: AxiosResponse<User.Response.FindOne>) =>
        this.mutations.setUser(response.data)
    );
  }
}
```


## vuex - getter
* 작성언어: typescript
* vuex 사용
```typescript
import {Getters} from 'vuex-smart-module';

import {User} from '@/src/app/sample/user/model/user.model';
import {UserState} from '@/store/user/user.state';
import {Mapper} from '@/src/core/service/mapper.service';

export class UserGetter extends Getters<UserState> {
  mapper = new Mapper();

  getUsers(): User.Response.FindAll[] {
    return this.mapper.toArray(User.Response.FindAll, this.state.users);
  }

  getUser(): User.Response.FindOne {
    return this.mapper.toObject(User.Response.FindOne, this.state.user);
  }
}

```


## vuex - mutation
* 작성언어: typescript
* vuex 사용
```typescript
import {Mutations} from 'vuex-smart-module';

import {User} from '@/src/app/sample/user/model/user.model';
import {UserState} from '@/store/user/user.state';
import {Mapper} from '@/src/core/service/mapper.service';

export class UserMutation extends Mutations<UserState> {
  mapper = new Mapper();

  setUsers(params: User.Response.FindAll[]) {
    this.state.users = this.mapper.toArray(User.Response.FindAll, params);
  }

  setUser(params: User.Response.FindOne) {
    this.state.user = this.mapper.toObject(User.Response.FindOne, params);
  }
}

```


## vuex - state
* 작성언어: typescript
* vuex 사용
```typescript
import {User} from '@/src/app/sample/user/model/user.model';

export class UserState {
  users: User.Response.FindAll[] = [];
  user: User.Response.FindOne = new User.Response.FindOne;
}

```

## vuex - module
* 작성언어: typescript
* vuex 사용
```typescript
import {Module} from 'vuex-smart-module';

import {UserState} from '@/store/user/user.state';
import {UserGetter} from '@/store/user/user.getter';
import {UserMutation} from '@/store/user/user.mutation';
import {UserAction} from '@/store/user/user.action';

export default new Module({
  state: UserState,
  getters: UserGetter,
  mutations: UserMutation,
  actions: UserAction,
});
```

## vuex - index
* 작성언어: typescript
* vuex 사용
* root에 키를 module을 추가
```typescript
import 'reflect-metadata';
import Vue from 'vue';
import Vuex from 'vuex';
import {Module} from 'vuex-smart-module';

import UserModule from '@/store/user/user.module';
import AlbumModule from '@/store/album/album.module';
import GlobalModule from '@/store/global/global.module';

Vue.use(Vuex);

const root = new Module({
  modules: {
    user: UserModule,
    album: AlbumModule,
    global: GlobalModule
  },
});

export const {
  state,
  getters,
  mutations,
  actions,
  modules,
  plugins
} = root.getStoreOptions();
```

## config
* 작성언어: typescript
* config 변수는 `SCREAMING_SNAKE_CASE`로 작성한다.
```typescript
export const VALIDATE_TOASTS = {
  isString: '자료형이 문자 형태여야 합니다.',
  isInt: '자료형이 정수 형태여야 합니다.',
  isNumber: '자료형이 정수 형태여야 합니다.',
  isNumberString: '자료형이 정수 형태여야 합니다.',
  isNotEmpty: '필수 정보 입니다.',
  isArray: '자료형이 배열 형태여야 합니다.',
  arrayNotEmpty: '배열에 데이터가 누락 되었습니다.',
  maxLength: '최대 입력 범위를 초과 하였습니다.',
  isEmail: '이메일 정보를 올바르게 입력해 주세요.',
  isHexColor: '색상 정보를 올바르게 입력해 주세요.'
};
```

## service
* 작성언어: typescript
* service는 공통으로 사용하는 함수 및 변수만 작성한다.
```typescript
import get from 'lodash/get';
import moment from 'moment-timezone';
import {Enum} from '@/src/app/shared/model/enum.model';

export default class UtilService {
  safeCall(parent: any, key: string) {
    return get(parent, key);
  }

  dateToFormatDate(date: Date | string, format: Enum.CORE.DATE_FORMAT = Enum.CORE.DATE_FORMAT.DISPLAY_DATE_TIME): string {
    return moment(date).format(format);
  }

  setNumberToString(num: number): string {
    let returnValue = '';

    if (num !== null && num !== undefined) {
      if (num >= 10) {
        returnValue = num.toString();
      } else {
        returnValue = `0${num}`;
      }
    }

    return returnValue;
  }

  getRandomString(length: number) {
    let result: string = '';
    const characters: string = '0123456789';
    const charactersLength: number = characters.length;

    for (let i = 0; i < length; i++) {
      result += characters.charAt(Math.floor(Math.random() * charactersLength));
    }

    return `Random_${result}`;
  }
}
```

## server
* 작성언어: typescript
* SSR 구현영역
```typescript
export namespace OtherServer {
  export const Index = {
    async asyncData({store}: any) {
      await store.dispatch('global/setToken', 'TEST_GLOBAL_SAMPLE');

      console.log('SERVER GLOBAL: ', store.getters['global/getToken']());
      return {};
    }
  };

  export const Middleware = {
    middleware: ['check.middleware']
  }

  export const Server = {
    async asyncData({store}: any) {
      try {
        await store.dispatch('user/getUsers');
        console.log('SERVER GLOBAL: ', store.getters['global/getToken']());
        await store.dispatch('user/getUser', '1');
      } catch (error) {
        console.log('ERROR', error);
      }

      return {
        users: store.getters['user/getUsers'](),
        user: store.getters['user/getUser']()
      };
    }
  }
}
```
